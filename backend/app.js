const express = require('express')
const cors = require('cors')
const compression = require('compression')
const helmet = require('helmet')

const path = require('path');
const fs = require('fs');

const XLSX = require('xlsx');

const port = 3002

const corsOptions = {
    exposedHeaders: 'Content-Dispositon',
};

const app = express()
app.use(cors(corsOptions))
app.use(compression())
app.use(helmet())
app.use(express.json());
app.use( '/', express.static(path.join(__dirname, 'public')))

app.get('/hello', (req, res) => { 
    console.log('Hello World')
    res.send('Hello World!')
})

app.get('/projects', async (req, res, next) => {
    let project_list = [];
    const directoryPath = path.join(__dirname, 'projects');
    //passsing directoryPath and callback function
    await new Promise( (resolve) => {
        fs.readdir(directoryPath, function (err, files) {
            if (err) {
                return console.log('Unable to scan directory: ' + err);
            } 

            
            //listing all files using forEach
            files.forEach(function (file) {
                // Do whatever you want to do with the file
                console.log(file); 
                project_list.push( file );
            });
            
            resolve()
        });
    } )
    res.json({msg: 'SUCCESS', result: project_list })
})

app.get('/projects/:category/config', async (req, res, next) => {
    let config = {};
    const configFile = path.join(__dirname, `projects/${req.params.category}/.config`);
    //passsing directoryPath and callback function
    await new Promise( (resolve) => {

        config = JSON.parse(fs.readFileSync(configFile));
        console.log( config )
            
        resolve()
    } )
    res.json({msg: 'SUCCESS', result: config })
})

app.get('/projects/:category/images/random', async (req, res, next) => {
    const directoryPath = path.join(__dirname, `projects/${req.params.category}/images`);
    let file = undefined;
    //passsing directoryPath and callback function
    await new Promise( (resolve) => {
        fs.readdir(directoryPath, function (err, files) {
            //handling error
            if (err) {
                return console.log('Unable to scan directory: ' + err);
            } 

            const size = files.length;
            file = files[Math.floor(Math.random() * size)];
            resolve()
        });
    } )

    //console.log( file )
    res.setHeader("Content-Dispositon","attachment; filename="+ file);
    res.sendFile(directoryPath +'/'+ file);
})

app.post('/projects/:category/images/:image/categorize', function (req, res, next) {

    const { image, category } = req.params;
    const { choices } = req.body;

    const projectPath = path.join(__dirname, `projects/${category}`);
    const config = JSON.parse( fs.readFileSync(projectPath+'/.config') );

    if( !fs.existsSync(`${projectPath}/images/${image}`) ){
        res.json({ msg: 'Image not found.' })
        return;
    }

    if( !fs.existsSync(`${projectPath}/categorize`) ){
        fs.mkdirSync(`${projectPath}/categorize`)
    }

    /*
    NEXT PHASE FOR SUB CATE AS MULTIPLE PATH
    config.sub_category.forEach( (sub_cate, idx) => {

        if( !choices[sub_cate] ){
            res.json({ msg: 'Main Category not found.' })
            return;
        }

        const sub_cat_path = config.sub_category.slice(0, idx+1).join('/');
        if( !fs.existsSync(`${projectPath}/categorize/${sub_cat_path}`) ){
            fs.mkdirSync(`${projectPath}/categorize/${sub_cat_path}`)
        }
    })
    */

    if( !fs.existsSync(`${projectPath}/categorize/${choices[config.sub_category[0]]}`) ){
        fs.mkdirSync(`${projectPath}/categorize/${choices[config.sub_category[0]]}`)
    }

    fs.renameSync(`${projectPath}/images/${image}`, `${projectPath}/categorize/${choices[config.sub_category[0]]}/${image}`);
    
    res.json({ msg: 'SUCCESS' })
})


app.listen(port, () => console.log(`Example app listening at http://localhost:${port}`))