const path = require('path');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');

module.exports = {
    entry: './app.js',
    output: {
        filename: 'main.js',
        path: path.resolve(__dirname, 'build'),
    },
    node: {
        fs: 'empty',
        __dirname: false,
        __filename: false,
    },
    target: 'node',
    optimization: {
        minimize: false
        /*
        minimizer: [
            new UglifyJsPlugin({
                uglifyOptions: {
                    mangle: false
                },
            })
        ],
        */
    },
    plugins: [
        new CopyPlugin(
          [
            { from: './package.json', to: 'package.json' },
          ],
        ),
      ],
};