import React, { useEffect, useState } from "react"
import axios from 'axios'
import "./index.scss"

const url = `/projects`
//http://203.172.109.16:3002
//`http://192.168.2.218:3002/projects`
//`http://127.0.0.1:3002/projects`

const IndexPage = () => {

  const [ projects, setProjects ] = useState([])
  const [ project, setProject ] = useState(undefined)
  const [ config, setConfig ] = useState(undefined)
  const [ image, setImage ] = useState(undefined)
  const [ fileName, setFileName ] = useState(undefined)
  const [ endFlow, setEndFlow ] = useState(false)

  useEffect( () => {
    (async () => {
      try{
        let {data} = await axios.get(`${url}`)
        console.log( 'result', data )
        setProjects(data.result)
      }catch(e){
        console.error(e)
      }
    })();
  }, [])

  useEffect( () => {
    getConfig()
    randomImg()
  }, [project])

  const getConfig = async () => {
    if( !project ) return
    let {data} = await axios.get(`${url}/${project}/config`)
    console.log( 'result', data )
    setConfig( data.result )
  }

  const randomImg = async () => {
    if( !project ) return
    try{
      let resp = await axios.get(`${url}/${project}/images/random`, {responseType: 'arraybuffer'})
      console.log( resp['headers']['content-dispositon'] )
      //console.log( Buffer.from(resp.data, 'binary').toString('base64')  )
      //setImage(img)
      setImage( 'data:image/png;base64,'+ Buffer.from(resp.data, 'binary').toString('base64') )
      setFileName( resp['headers']['content-dispositon'].replace("attachment; filename=", "") )
    }catch(e){
      //console.log( e.response.status )
      if( e.response.status ){
        setEndFlow(true)
      }
    }
  }

  const processCategorize = async ( selected ) => {
    let body = {
      "choices":{
        [`${config.sub_category[0]}`]: selected
      }
    }
    let {data} = await axios.post(`${url}/${project}/images/${fileName}/categorize`, body)
    randomImg()
    console.log( 'result', data )
  }

  const processProject = (project) => {
    console.log( 'project', project )
    setProject(project)
  }

  const getChiocesProp = () => {
    //console.log( config.sub_category )
    let chioceIdx = config.choices.filter( (cfg,idx) => {
      //console.log( Object.keys(cfg)[0] )
      return Object.keys(cfg)[0] == config.sub_category[0] ? true : false
    })[0]
    //console.log( chioceIdx )
    //console.log( chioceIdx[config.sub_category[0]] )
    return chioceIdx[config.sub_category[0]] || []
  }

  return(
    <div style={{ textAlign: 'center', margin: '16px' }}>
      { !project &&
        <>
        <h3 className="title">Select Project</h3>
        {projects.map( (project, idx) => {
          return(
            <div key={'project'+idx}>
              <div className={'tag'} onClick={ () => processProject(project) } style={{ margin: '8px 0', cursor: 'pointer' }}>{project}</div>
              <br/>
            </div>)
        } )}
        </>
      }
      {
        project && ( endFlow ?
        <>
          <h3 className="title">Current Project is [{project}] </h3>
          <div> Image is empty. </div>
        </>
        :
        <>
          <h3 className="title">Current Project is [{project}] </h3>
          {/*<button onClick={randomImg}> Random </button> */}
          <img src={image} width={'50%'} style={{ maxWidth: '30vw', maxHeight: '70vh', objectFit: 'cover' }} />
          <br/>
          {
            config && config.choices &&
            getChiocesProp().map( (choice,idx) => {
              return <button 
                  key={'choice'+idx} 
                  className="button is-primary is-light" 
                  style={{ margin: '0 8px' }}
                  alt={choice}
                  onClick={ () => processCategorize(choice) }
                >
                  {choice} 
                </button>
            })
          }
        </>
        )
      }
    </div>
  );

}

export default IndexPage
